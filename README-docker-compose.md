# Notes: openvpn docker-compose

docker-compose basic config for create private VPN on VPS 
from URL: https://github.com/kylemanna/docker-openvpn/blob/master/docs/docker-compose.md

## Getting started
```
mkdir openvpn/vpn-data -p
cd ./openvpn
export vps_ip_address=222.222.222.222
export client_name=example_name
```

# Initialize the configuration files and certificates
```
docker-compose run --rm openvpn ovpn_genconfig -u udp://${vps_ip_address}
docker-compose run --rm openvpn ovpn_initpki
```

# Start OpenVPN server process
```
docker-compose up -d openvpn
```

# Generate a client certificate
- with a passphrase (recommended)
```
docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME
```
- without a passphrase (not recommended)
```
docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME nopass
```

# Retrieve the client configuration with embedded certificates
docker-compose run --rm openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn

# Revoke a client certificate
- Remove the corresponding crt, key and req files.
```
docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME remove
```

# access the container logs with
```
docker-compose logs -f
```
